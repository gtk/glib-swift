import PackageDescription

#if os(OSX)
let gtkUrl = "git@gitlab.com:gtk/cgtk-osx.git"
#else
let gtkUrl = "../cgtk"
#endif


let package = Package(
	name: "GLib",
	dependencies:
	[
		.Package(url: gtkUrl, majorVersion: 1),
	]
)
