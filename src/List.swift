import CGTK

public extension Array {
	public init(list: UnsafeMutablePointer<GList>?) {
		var data = [Element]()
		
		var n_List = list

		if n_List?.pointee.data != nil {

			repeat {
				data.append(unsafeBitCast(n_List!.pointee.data, to: Element.self))
				n_List = n_List?.pointee.next
			} while n_List != nil
			
		}

		self.init(data)
	}
	
	public init(pointerList: UnsafeMutablePointer<GList>?) {
		var data = [Element]()
		
		var n_List = pointerList

		if n_List?.pointee.data != nil {

			repeat {
				data.append(unsafeBitCast(n_List!.pointee, to: Element.self))
				n_List = n_List!.pointee.next
			} while n_List != nil
			
		}

		self.init(data)
	}
}